from Arduino import Arduino
import paho.mqtt.client as mqtt
import time

board = Arduino('9600')
board.pinMode(13, "OUTPUT")

mqttc = mqtt.Client("arduino_publisher")
mqttc.connect("127.0.0.1", 1883)

while True:
    board.digitalWrite(13, "LOW")
    print "publish message " + "LED off"
    mqttc.publish("arduino_mqtt", "LED off")
    time.sleep(1)
    
    board.digitalWrite(13, "HIGH")
    print "publish message " + "LED on"
    mqttc.publish("arduino_mqtt", "LED on")
    time.sleep(1)

mqttc.loop_forever()