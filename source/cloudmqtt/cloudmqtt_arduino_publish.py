import paho.mqtt.client as mqtt
import time
import datetime
from Arduino import Arduino

board = Arduino('9600')
mqttc = mqtt.Client("cloudmqtt_arduino_publish")

#Pass authentication parameters to cloudmqtt
mqttc.username_pw_set("_your_username", password="_your_password")
mqttc.connect("_instance.cloudmqtt.com", 10199)

#Analog pin 0- light sensor (ldr) 
#Analog pin 1- temperature sensor (LM35)

#last time of transmission
lastSend = None
light_data = 0
temp_data = 0

while True:
    
    now = datetime.datetime.utcnow()

    #set read rate from analog pins to 2 seconds
    time.sleep(2) 
    light_data = board.analogRead(0)
    # constrain values to range 0-255
    light_data_c = light_data / 4 

    temp_data = board.analogRead(1)
    temp_data_c= (5.0 * temp_data * 100.0)/1024.0

    if lastSend is None or now - lastSend > datetime.timedelta( seconds = 3 ):

		data = '{ "data" : { "light_level": %.2f , "temperature": %.2f , "timestamp": %s } }' % (light_data_c, temp_data_c, now.isoformat()) 

        mqttc.publish("sensorData", data)
        print "publish message " + data 
        time.sleep(1)
        lastSend=now

mqttc.loop_forever()

board.close()